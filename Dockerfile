FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > syslog-ng.log'

COPY syslog-ng .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' syslog-ng
RUN bash ./docker.sh

RUN rm --force --recursive syslog-ng
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD syslog-ng
